package dam.androidedgar.u3t3menuofactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ItemDetailActivity extends AppCompatActivity {
    TextView tv4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setUI();
        escribir();

    }

    private void setUI() {
        tv4 = findViewById(R.id.tv4);
    }

    private void escribir() {
        Bundle parametros = this.getIntent().getExtras();
        String nombre = parametros.getString("nombre");
        tv4.setText(nombre);
        setTitle(nombre);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        return true;
    }
}
