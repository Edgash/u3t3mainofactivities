package dam.androidedgar.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    public interface OnItemClickListener{
        void onItemClick(String activityName);
    }

    private String[] myDataSet;
    private OnItemClickListener listener;

    MyAdapter(String[] myDataSet, OnItemClickListener listener){
        this.myDataSet = myDataSet;
        this.listener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public MyViewHolder(TextView textView){
            super(textView);
            this.textView = textView;
        }

        public void bind(String activityName, OnItemClickListener listener){
            this.textView.setText(activityName);
            this.textView.setOnClickListener(v -> listener.onItemClick(textView.getText().toString()));
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        TextView tv = (TextView) LayoutInflater.from (parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false);
        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position){
        viewHolder.bind(myDataSet[position], listener);
    }

    @Override
    public int getItemCount(){
        return myDataSet.length;
    }

    public interface onItemClickListener{
        void onItemClick(String activityName);
    }
}