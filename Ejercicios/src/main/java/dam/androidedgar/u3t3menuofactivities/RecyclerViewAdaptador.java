package dam.androidedgar.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdaptador extends RecyclerView.Adapter<RecyclerViewAdaptador.ViewHolder> {
    private OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(List<VersionAndroid> version, int position);

        void onClick(View view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView version, api, nombre;
        ImageView fotoAndroid;
        CardView infoAndroid;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.txtVersion);
            api = (TextView) itemView.findViewById(R.id.txtApi);
            version = (TextView) itemView.findViewById(R.id.txtNombre);
            fotoAndroid = (ImageView) itemView.findViewById(R.id.imgAndroid);
            infoAndroid = itemView.findViewById(R.id.infoAndroid);
        }


        public void bind(List<VersionAndroid> version, OnItemClickListener listener, int position){
            this.infoAndroid.setOnClickListener(v -> listener.onItemClick(version, position));
        }
    }

    public List<VersionAndroid> AndroidLista;

    public RecyclerViewAdaptador(List<VersionAndroid> versionAndroid) {
        AndroidLista = versionAndroid;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.version.setText(AndroidLista.get(position).getVersion());
        holder.api.setText(AndroidLista.get(position).getApi());
        holder.nombre.setText(AndroidLista.get(position).getNombre());
        holder.fotoAndroid.setImageResource(AndroidLista.get(position).getImgAndroid());
        holder.bind(AndroidLista, listener, position);
    }

    @Override
    public int getItemCount() {
        return AndroidLista.size();
    }

}
