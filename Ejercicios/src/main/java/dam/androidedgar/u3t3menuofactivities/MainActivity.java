package dam.androidedgar.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements RecyclerViewAdaptador.OnItemClickListener{
    private List<VersionAndroid> version;
    private RecyclerView recyclerViewAndroid;
    private RecyclerViewAdaptador adaptadorAndroid;
    private ImageView senyal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

    }

    @SuppressLint("WrongViewCast")
    private void setUI() {
        recyclerViewAndroid = (RecyclerView)findViewById(R.id.recyclerAndroid);
        recyclerViewAndroid.setLayoutManager(new LinearLayoutManager(this));

        adaptadorAndroid = new RecyclerViewAdaptador(obtenerVersiones());
        recyclerViewAndroid.setHasFixedSize(true);
        recyclerViewAndroid.setLayoutManager(new LinearLayoutManager(this));
        new ItemTouchHelper(itemToucheHelperCallback).attachToRecyclerView(recyclerViewAndroid);
        recyclerViewAndroid.setAdapter(adaptadorAndroid);
    }

    public List<VersionAndroid> obtenerVersiones(){
        version = new ArrayList<>();
        version.add(new VersionAndroid("Version: 12", "API: 31", "Android 12", R.drawable.android12));
        version.add(new VersionAndroid("Version: 11", "API: 30", "Android 11", R.drawable.android11));
        version.add(new VersionAndroid("Version: 10", "API: 29", "Android 10", R.drawable.android10));
        version.add(new VersionAndroid("Version: 9", "API: 28", "Pie", R.drawable.android9));
        version.add(new VersionAndroid("Version: 8", "API: 26", "Oreo", R.drawable.android8));
        version.add(new VersionAndroid("Version: 7", "API: 24", "Nougat", R.drawable.android7));
        version.add(new VersionAndroid("Version: 6", "API: 23", "Marshmallow", R.drawable.android6));
        version.add(new VersionAndroid("Version: 5", "API: 21", "Lollipop", R.drawable.android5));

        return version;
    }

    ItemTouchHelper.SimpleCallback itemToucheHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            version.remove(viewHolder.getAdapterPosition());
            adaptadorAndroid.notifyDataSetChanged();
            if(version.size() == 0){
                senyal = findViewById(R.id.imgSenyal);
                senyal.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onItemClick(List<VersionAndroid> version, int position) {
        Intent i = new Intent(MainActivity.this, ItemDetailActivity.class);
        i.putExtra("version", version.get(position).getVersion());
        i.putExtra("api", version.get(position).getApi());
        i.putExtra("nombre", version.get(position).getNombre());
        i.putExtra("imgAndroid", version.get(position).getImgAndroid());
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                break;
            case R.id.btnDeleteAll:
                adaptadorAndroid = new RecyclerViewAdaptador(version);
                recyclerViewAndroid.setHasFixedSize(true);
                recyclerViewAndroid.setLayoutManager(new LinearLayoutManager(this));
                recyclerViewAndroid.setAdapter(adaptadorAndroid);
                senyal.setVisibility(View.VISIBLE);
                break;
            case R.id.btnRestore:
                setUI();
                senyal.setVisibility(View.INVISIBLE);
                break;
        }
    }
}