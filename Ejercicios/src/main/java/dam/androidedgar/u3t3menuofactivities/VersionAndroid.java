package dam.androidedgar.u3t3menuofactivities;

public class VersionAndroid {

    private String version, api, nombre;
    private int imgAndroid;


    public VersionAndroid() {
    }

    public VersionAndroid(String version, String api, String nombre, int imgAndroid) {
        this.version = version;
        this.api = api;
        this.nombre = nombre;
        this.imgAndroid = imgAndroid;
    }

    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApi() {
        return api;
    }
    public void setApi(String version) {
        this.api = api;
    }

    public int getImgAndroid() {
        return imgAndroid;
    }
    public void setImgAndroid(int imgAndroid) {
        this.imgAndroid = imgAndroid;
    }
}
