package dam.androidedgar.u3t3menuofactivities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class ItemDetailActivity extends AppCompatActivity {
    CardView info;
    TextView version, api, nombre;
    ImageView imgAndroid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_adapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setUI();
        escribir();
    }

    @SuppressLint("WrongViewCast")
    private void setUI() {
        info = findViewById(R.id.info);
        version = findViewById(R.id.txtVersion);
        api = findViewById(R.id.txtApi);
        nombre = findViewById(R.id.txtNombre);
        imgAndroid = findViewById(R.id.imgAndroid);

    }

    private void escribir() {
        Bundle parametros = this.getIntent().getExtras();
        String txtVersion = parametros.getString("version");
        String txtApi = parametros.getString("api");
        String txtNombre = parametros.getString("nombre");
        int fotoAndroid = parametros.getInt("imgAndroid");

        version.setText(txtVersion);
        api.setText(txtApi);
        nombre.setText(txtNombre);
        imgAndroid.setImageResource(fotoAndroid);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(myIntent);
        return true;
    }


}
